<?php
include 'header.php';
?>
<h1><a name="wadseeker"></a>Wadseeker API Documentation</h1>
<ul>
	<li><a href="docs/wadseeker_2.2.3/">Wadseeker API 2.2.3</a></li>
	<li><a href="docs/wadseeker_2.1.3/">Wadseeker API 2.1.3</a></li>
	<li><a href="docs/wadseeker_2.1.2/">Wadseeker API 2.1.2</a></li>
	<li><a href="docs/wadseeker_2.1/">Wadseeker API 2.1</a></li>
	<li><a href="docs/wadseeker_2.0/">Wadseeker API 2.0</a></li>
	<li><a href="docs/wadseeker_1.2/">Wadseeker API 1.2</a></li>
	<li><a href="docs/wadseeker_1.1/">Wadseeker API 1.1</a></li>
	<li><a href="docs/wadseeker_1.0/">Wadseeker API 1.0</a></li>
	<li><a href="docs/wadseeker_0.7/">Wadseeker API 0.7</a></li>
	<li><a href="docs/wadseeker_0.6/">Wadseeker API 0.6</a></li>
	<li><a href="docs/wadseeker_0.5/">Wadseeker API 0.5</a></li>
	<li><a href="docs/wadseeker_0.4/">Wadseeker API 0.4</a></li>
	<li><a href="docs/wadseeker_0.3/">Wadseeker API 0.3</a></li>
	<li><a href="docs/wadseeker_0.2/">Wadseeker API 0.2</a></li>
	<li><a href="docs/wadseeker_0.1/">Wadseeker API 0.1</a></li>
</ul>
<h1>Doomseeker Plugins</h1>
<ul>
	<li style="font-weight: bold"><a href="docs/plugins.php">Startup Tutorial</a></li>
	<li><a href="docs/doomseeker_1.5.0/">Doomseeker API 1.5.0</a></li>
	<li><a href="docs/doomseeker_1.4.0/">Doomseeker API 1.4.0</a></li>
	<li><a href="docs/doomseeker_1.3.3/">Doomseeker API 1.3.3</a></li>
	<li><a href="docs/doomseeker_1.3.2/">Doomseeker API 1.3.2</a></li>
	<li><a href="docs/doomseeker_1.3/">Doomseeker API 1.3</a></li>
	<li><a href="docs/doomseeker_1.2/">Doomseeker API 1.2</a></li>
	<li><a href="docs/doomseeker_1.1/">Doomseeker API 1.1</a></li>
	<li><a href="docs/doomseeker_1.0/">Doomseeker API 1.0</a></li>
	<li><a href="docs/doomseeker_0.10/">Doomseeker API 0.10</a></li>
	<li><a href="docs/doomseeker_0.8/">Doomseeker API 0.8</a></li>
</ul>
<?php
include 'footer.php';
?>
