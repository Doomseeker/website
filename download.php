<?php
include 'header.php';
define('APTLOCATION', 'http://debian.drdteam.org');
define('PATH', '/files/');
define('DS_VERSION', '1.5.1');
define('DS_WS_VERSION', '2.2.3');
define('WS_VERSION', '2.2.3');

$osList = array(
	array('key' => 'macosx', 'pattern' => '/Mac/', 'name' => 'macOS'),
	array('key' => 'ubuntu', 'pattern' => '/Ubuntu/', 'name' => 'Ubuntu'),
	array('key' => 'windows', 'pattern' => '/Windows/', 'name' => 'Windows'),
	array('key' => 'source', 'pattern' => NULL, 'name' => 'Other (Source Code)')
);
if(!isset($_POST['os']))
{
	$os = 'source';
	foreach($osList as &$osItem)
	{
		if(preg_match($osItem['pattern'], $_SERVER['HTTP_USER_AGENT']))
		{
			$os = $osItem['key'];
			break;
		}
	}
}
else
{
	$os = $_POST['os'];
}

if ($os == "windows")
{
	define('DS_PLUGINUPDATE', 0);
}
else
{
	define('DS_PLUGINUPDATE', 0);
}

?>
<h1 style="text-align: center">Download Doomseeker <?php echo DS_VERSION; ?></h1>
<form method="post" action="#" id="osForm" style="text-align: center">
<div>Operating System: <select id="os" name="os">
<?php
foreach($osList as &$osItem)
{
	echo "<option value=\"{$osItem['key']}\"";
	if($os === $osItem['key'])
		echo ' selected="selected"';
	echo ">{$osItem['name']}</option>";
}
?>
</select> <noscript><input type="submit" value="Select" /></noscript></div>
</form>

<?php
if($os == 'source') {
?>
<h2>Doomseeker Source</h2>
<ul><li><a href="<?php echo PATH.'doomseeker-'.DS_VERSION.'.tar.xz'; ?>">Download <?php echo DS_VERSION; ?></a></li>
<li><a href="https://bitbucket.org/Doomseeker/doomseeker/get/<?php echo DS_VERSION; ?>.tar.bz2">Bitbucket Snapshot</a></li>
<?php
if(DS_PLUGINUPDATE > 0) {
?>
<li><a href="https://bitbucket.org/Doomseeker/doomseeker/get/<?php echo DS_VERSION.'-p'.DS_PLUGINUPDATE.'.tar.bz2'; ?>">Plugin updates source</a></li>
<?php
}
?>
</ul>
<p>Note that for plugin updates you will need to download aforementioned source from our repository, compile, and install just the shared object files over your <?php echo DS_VERSION; ?> installation.</p>
<p>Specific compilation instructions and dependency information can be found on our <a href="git.php">Git</a> page.</p>
<h2>Wadseeker Source</h2>
<ul><li><a href="<?php echo PATH.'libwadseeker-'.WS_VERSION.'.tar.xz'; ?>">Download <?php echo WS_VERSION; ?></a></li></ul>
<p>The Wadseeker <?php echo DS_WS_VERSION; ?> source is also included within the Doomseeker source download.</p>
<?php
} elseif($os == 'macosx') {
?>
<h2>Doomseeker for macOS</h2>
<ul><li><a href="<?php echo PATH.'doomseeker-'.DS_VERSION.'_macosx.dmg'; ?>">Download <?php echo DS_VERSION; ?></a></li>
<?php
if(DS_PLUGINUPDATE > 0) {
?>
<li><a href="<?php echo PATH.'doomseeker-'.DS_VERSION.'-p'.DS_PLUGINUPDATE.'_macosx.dmg'; ?>">Plugin update <?php echo DS_PLUGINUPDATE; ?></a></li>
<?php
}
?>
</ul>
<p>The macOS version of Doomseeker is available for macOS 10.13 High Sierra or later.</p>
<?php
if(DS_PLUGINUPDATE > 0) {
?>
<p>If you have already installed Doomseeker <?php echo DS_VERSION; ?> then you will need to download and install the plugin update. This is an incremental update and includes only the updates files. If you do not have Doomseeker installed then you must download both to install Doomseeker.</p>
<?php
}
?>
<?php
} elseif($os == 'ubuntu') {
?>
<h2>Doomseeker for Ubuntu</h2>
<p>Doomseeker may be installed through the package manager by adding the DRD Team package repository. Use the following command to add the repository to your sources list:</p>
<pre>sudo wget -O /etc/apt/trusted.gpg.d/drdteam.gpg <a href="https://debian.drdteam.org/drdteam.gpg">https://debian.drdteam.org/drdteam.gpg</a>
sudo apt-add-repository 'deb <a href="https://debian.drdteam.org">https://debian.drdteam.org/</a> stable multiverse'</pre>
<p>You may then proceed to install the package <i>doomseeker-&lt;port name&gt;</i> for any port that you desire to use. Wadseeker packages are also provided as <i>libwadseeker</i> and <i>libwadseeker-dev</i>. For example:</p>
<pre>sudo apt-get install doomseeker-chocolatedoom</pre>
<p>Doomseeker will be available in the games category after installation. Ubuntu 18.04 LTS and later are supported for both 32-bit and 64-bit systems.</p>
<?php
} elseif($os == 'windows') {
?>
<h2>Doomseeker for Windows</h2>
<ul><li><a href="<?php echo PATH.'doomseeker-'.DS_VERSION.(DS_PLUGINUPDATE > 0 ? '-p'.DS_PLUGINUPDATE : '').'_windows.zip'; ?>">Download Complete <?php echo DS_VERSION; ?> <?php if(DS_PLUGINUPDATE > 0) { echo 'Plugin Update '.DS_PLUGINUPDATE.' '; } ?>Package</a></li>
<li><a href="<?php echo PATH.'libwadseeker-'.WS_VERSION.'_src.zip'; ?>">Wadseeker Development Kit</a></li>
</ul>
<h2>Plugins</h2>
<ul>
<li><a href="download_plugin.php?plugin=chocolatedoom">Chocolate-Doom Plugin</a></li>
<li><a href="download_plugin.php?plugin=odamex">Odamex Plugin</a></li>
<li><a href="download_plugin.php?plugin=zandronumq">Q-Zandronum Plugin</a></li>
<li><a href="download_plugin.php?plugin=turok2ex">Turok 2 Remaster</a></li>
<li><a href="download_plugin.php?plugin=zandronum">Zandronum Plugin</a></li>
<?php /*<li><a href="download_plugin.php?plugin=wadseeker">Wadseeker <?php echo WS_VERSION; ?></a></li> */ ?>
</ul>
<?php
if(DS_PLUGINUPDATE > 0) {
?>
<p>Plugins in bold have been updated in the latest plugin update. You can download them individually to update incrementally if you have <?php echo DS_VERSION; ?> installed.</p>
<?php
}
?>
<?php
}
?>

<h2>Old Versions</h2>
<p>If you are looking for an older release of Doomseeker then you can browse the <a href="<?php echo PATH; ?>">files directory</a> listing.</p>

<?php
if($os == 'windows' || $os == 'macosx') {
?>
<h2>Git Builds</h2>
<p>From time to time development builds are also uploaded to the <a href="https://devbuilds.drdteam.org/doomseeker/">DRD Team Development Builds</a>
site.</p>
<?php
}
?>

<script type="text/javascript">
document.getElementById("os").onchange = function() {document.getElementById("osForm").submit()};
</script>
<?php
include 'footer.php';
?>
