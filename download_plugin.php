<?php
define('DSZIP', 'doomseeker-1.5.1_windows.zip');
if(!isset($_GET['plugin']) || !is_string($_GET['plugin']) || !ctype_alnum($_GET['plugin']))
	die('Invalid plugin.');

$zip = new ZipArchive;
if($zip->open('files/'.DSZIP) === true)
{
	if(strcmp($_GET['plugin'], 'wadseeker') == 0)
		$plug = $zip->getFromName('libwadseeker.dll');
	else
		$plug = $zip->getFromName('engines/lib'.$_GET['plugin'].'.dll');
	$zip->close();
	if($plug === false)
		die('Could not locate plugin!');

	header('Content-disposition: attachment; filename=lib'.$_GET['plugin'].'.dll');
	header('Content-type: application/x-msdownload');
	if(strpos($_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip') !== false)
	{
		header('Content-encoding: gzip');
		print(gzencode($plug));
	}
	else
		print($plug);
}
else
	die('Error opening zip archive!');
?>
