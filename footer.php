<div class="bot-bar"><span class="left">&nbsp;</span><span class="right">&nbsp;</span>&nbsp;</div>
<div class="copyright">Doomseeker &copy; 2009-2024 The Doomseeker Team</div>
<script type="text/javascript">
<!--
var _gaq = _gaq || [];
_gaq.push(['_setAccount', 'UA-28900138-2']);
_gaq.push(['_trackPageview']);
(function() {
	var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
})();
// -->
</script>
</body>
</html>
<?php
exit();
?>
