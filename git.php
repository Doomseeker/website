<?php
include 'header.php';
?>
<h1>Git Repository</h1>
<p>The latest version of the Doomseeker source code is publicly available 
through our git repository on Bitbucket. The repository is available 
at the following location:</p>
<p><a href="https://bitbucket.org/Doomseeker/doomseeker">https://bitbucket.org/Doomseeker/doomseeker</a></p>
<h1>Prebuilt Binaries</h1>
<p>On occasion binaries of Git revisions for Windows and macOS are provided 
for testing purposes. These builds can be found on the DRD Team 
<a href="https://devbuilds.drdteam.org/doomseeker/">development builds</a> website.</p>
<h1>Compilation Instructions</h1>
<p>To compile you will need CMake, the Qt Toolkit, libbz2, and zlib. 
These can be found on  <a href="https://cmake.org/">cmake.org</a>,
<a href="https://www.qt.io/">qt.io</a>, <a href="http://bzip.org/">bzip2.org</a>, 
and <a href="https://zlib.net/">zlib.net</a> respectively. On Windows mingw64 is supported and 
instructions are available in the <a href="https://bitbucket.org/Doomseeker/doomseeker/src/master/COMPILE.Windows.txt">repository</a>.</p>
<p>On Debian based Linux distributions the required software can be retrieved 
by the following command:</p>
<pre>sudo apt install build-essential cmake git libbz2-dev zlib1g-dev \
  qt5-default qttools5-dev qttools5-dev-tools qtmultimedia5-dev</pre>
<p>Regardless of your preferred distribution the source can be retrieved and 
compiled with the following command sequence:</p>
<pre>git clone https://bitbucket.org/Doomseeker/doomseeker.git doomseeker
cd doomseeker
mkdir build
cd build
cmake ..
make
sudo make install</pre>
<p>If the last command was sucessful you should now be able to start Doomseeker 
by typing doomseeker from the terminal.</p>
<?php
include 'footer.php';
?>
