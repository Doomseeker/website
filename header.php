<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Doomseeker - Cross platform Doom server browser.</title>
<link rel="stylesheet" type="text/css" href="<?php if(defined('URL_PREFIX')) { echo URL_PREFIX; } ?>styles.css" />
<link rel="stylesheet" type="text/css" href="<?php if(defined('URL_PREFIX')) { echo URL_PREFIX; } ?>doxygen.css" />
<?php
if(strpos($_SERVER['PHP_SELF'], 'docs/doomseeker_') !== false
	|| strpos($_SERVER['PHP_SELF'], 'docs/wadseeker_') !== false) {
?>
<script type="text/javascript" src="jquery.js"></script>
<script type="text/javascript" src="dynsections.js"></script>
<link href="search/search.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="search/search.js"></script>
<script type="text/javascript">
  $(document).ready(function() { searchBox.OnSelectItem(0); });
</script>
<?php
}
?>
<link rel="icon" type="image/vnd.microsoft.icon" href="<?php if(defined('URL_PREFIX')) { echo URL_PREFIX; } ?>favicon.ico" />
<meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
</head>
<body>
<div><a href="<?php if(defined('URL_PREFIX')) { echo URL_PREFIX; } ?>./"><img src="<?php if(defined('URL_PREFIX')) { echo URL_PREFIX; } ?>images/logo.png" alt="Doomseeker" class="logo" /></a></div>
<div class="bar">
<span class="left">&nbsp;</span>
<span class="right">&nbsp;</span>
<ul>
	<li><a href="<?php if(defined('URL_PREFIX')) { echo URL_PREFIX; } ?>./">Doomseeker</a></li>
	<li><a href="<?php if(defined('URL_PREFIX')) { echo URL_PREFIX; } ?>wadseeker.php">Wadseeker</a></li>
	<li><a href="<?php if(defined('URL_PREFIX')) { echo URL_PREFIX; } ?>download.php">Download</a></li>
	<li><a href="http://zandronum.com/tracker/search.php?project_id=2">Tracker</a></li>
	<li><a href="<?php if(defined('URL_PREFIX')) { echo URL_PREFIX; } ?>git.php">Git</a></li>
	<li><a href="<?php if(defined('URL_PREFIX')) { echo URL_PREFIX; } ?>docs.php">Docs</a></li>
</ul>
</div>
