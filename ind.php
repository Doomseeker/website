<?php
// Why ind.php? Someone, no idea who, had a virus which stole the ftp creds.
// It inserted some html code, but only at the bottom of files named index.*

include 'header.php';
?>
<div style="float: right;font-weight: bold">Latest Version: 1.5.1 (November 24, 2024)</div>
<h1>A Universal Browser</h1>
<p>Doomseeker is a cross-platform server browser for Doom. The goal of 
Doomseeker is to provide a smooth, consistant experience for Doom players 
regardless of port or platform. Doomseeker provides support for <a href="https://www.chocolate-doom.org">Chocolate 
Doom</a>, <a href="https://odamex.net">Odamex</a>,
<a href="http://qzandronum.com">Q-Zandronum</a> and <a href="https://zandronum.com">Zandronum</a>.
With its plugin system support for even more can be added.</p>
<p>Using the Qt Toolkit, Doomseeker can run on wide variety of platforms 
including Linux, macOS, and Windows. In addition the browser is open source 
and licensed under the GNU Lesser General Public License v2.1.</p>
<h1>The Fastest Way to Play Doom</h1>
<p>One feature that sets Doomseeker aside from most alternatives is its speed. 
Over 200 servers can be queried in about 2 seconds. Doomseeker ensures that you 
do not need to wait to get in the game. Just start up and play.
<img src="images/doomseeker.png" alt="" /></p>
<?php
include 'footer.php';
?>
