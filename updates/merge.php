<?php
$channels = array('beta', 'stable');
$plats = array('macosx', 'win32');
if(!isset($_GET['plat'])) die();

$basename = 'update-info_';
$valid = false;
foreach($plats as $plat)
{
	if($_GET['plat'] == $plat)
	{
		$basename .= $plat.'_';
		$valid = true;
		break;
	}
}

if(!$valid) die();

$data = array();
foreach($channels as $channel)
{
	$inData = json_decode(file_get_contents($basename.$channel.'.js'), true);
	foreach($inData as $key => &$ind)
	{
		$data[$key][$channel] = $ind;
	}
}
echo json_encode($data);
