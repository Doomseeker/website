<?php
include 'header.php';
?>
<div style="float: right;font-weight: bold">Latest Version: 2.2.3</div>
<h1>A Library To Find Mods</h1>
<p>Wadseeker is a library for making programs similar to GetWAD. Wadseeker uses 
a small subset of the Qt Toolkit, namely QtCore and QtNetwork, to provide an 
easy to use API.</p>
<p>Wadseeker is designed for reuse. A GUI is not provided as to allow the best 
possible integration with your own programs along with reducing the overhead of 
using it if you chose not to use Qt as your GUI Toolkit.
<img src="images/wadseeker.png" alt="" /></p>
<div style="text-align: center;font-size: 8pt">Wadseeker as implemented by Doomseeker</div>
<h1>Wadseeker at a Glance</h1>
<ul>
	<li>Searches common locations by default including the id games archive.</li>
	<li>Concurrent file downloading.</li>
	<li>7-zip archive support.</li>
	<li>Supports finding mods with any extension, but will also find them if compressed in a ZIP 
 or 7z archive.</li>
	<li>Contains a blacklist to prevent users from searching for IWADs.</li>
	<li>Integrates well with Qt GUIs.</li>
	<li>Licensed under the LGPL to allow use within non-opensource applications.</li>
</ul>
<h1>Resources</h1>
<ul>
	<li><a href="download.php">Download Wadseeker Development Kit</a></li>
	<li><a href="docs.php#wadseeker">Wadseeker API Documentation</a></li>
</ul>
<?php
include 'footer.php';
?>
